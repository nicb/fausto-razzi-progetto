; FAUSTO RAZZI
; ***********************************
; SINFONIA in RE orchestra
; **************************************
;
; $Id: completa.orc 1144 2008-07-06 06:40:08Z nicb $
;
; DPSDC Orchestra Driver, with configurable tables
;
sr=44100
kr=4410
ksmps=10
nchnls=2

    instr 30
    idur = p3
    iamp = ampdb(p4)
    ienvtable = p14
    imodtable = p15
    idccutoff=15
    ibasefrq = p5
    icutoff = 450
    ifrqexp = (ibasefrq >= icutoff ? 1.028 : 1)
    icarrier = p6
    imodulator = p7 * ifrqexp

kenv  oscil1i 0,iamp,idur,ienvtable
kmod  oscil1i 0,p10,idur,imodtable

acar  foscil  kenv,ibasefrq,icarrier,imodulator,kmod,1

acar  butterhp  acar,idccutoff
acar  butterhp  acar,idccutoff
acar  butterhp  acar,idccutoff

aleft = acar * (1-p12)
arite = acar * (p12)

    outs  aleft,arite
    endin

;     instr 31
;     idur = p3
;     iamp = ampdb(p4)
;     ienvtable = p14
;     imodtable = p15
;     idccutoff=15
;     imodfrq = p7*1.028
; 
; kenv  oscil1i 0,iamp,idur,ienvtable
; kmod  oscil1i 0,p10,idur,imodtable
; 
; acar  foscil  kenv,p5,p6,imodfrq,kmod,1
; 
; acar  butterhp  acar,idccutoff
; acar  butterhp  acar,idccutoff
; acar  butterhp  acar,idccutoff
; 
; aleft = acar * (1-p12)
; arite = acar * (p12)
; 
;     outs  aleft,arite
;     endin

    instr 1
	  iamp=ampdb(p4)/ampdb(60)
	  ilpan=(1-p6)*iamp
	  irpan=p6*iamp
aout    soundin p5

    aoutl = aout*ilpan
    aoutr = aout*irpan

	  outs   aoutl, aoutr
	  endin

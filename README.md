# Fausto Razzi *Progetto* per una composizione per elaboratore

The software needed to build the music composition *Progetto - per una composizione per elaboratore* by Fausto Razzi.

This piece was conceived when computers were at an early stage. This project
is an attempt to rebuild the piece using modern scripting languages (like
`ruby` or `python`) in order to make a final version.

The reference is the initial realization dating from the seventies.

## Status

Early stage.

## Requirements

There are currently no requirements.

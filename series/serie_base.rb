#
# $Id: serie_base.rb 68 2014-02-18 23:49:56Z nicb $
#

def calc_sequence
  p = 2.0
  n_tot = 5.0
	k = []

	(1..n_tot).each do
		|z|
		ptoz = p**z
		(1..(ptoz-1)).each do
			|m|
			k << (ptoz + m)/ptoz
		end
	end

	return k
end

def print_sequence
	res = calc_sequence
	res.sort.each { |k| puts("#{k}") }
end

print_sequence
